%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FLAGS TO PICK ALGORITHM OF CHOICE %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

IFD = 0; % INTER-FRAME DIFFERENCE
AMF = 1; % ADAPTIVE MEDIAN FILTERING
CMF = 0; % CHROMATICITY-BASED ADAPTIVE MEDIAN FILTERING
ARM = 0; % APPROXIMATE (RUNNING) MEDIAN FILTER
RGA = 0; % SELECTIVE RUNNING GAUSSIAN AVERAGE
NPP = 0; % NON-PARAMETRIC PROBABILITY BASED DETECTION

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FLAGS FOR DRAWING POINTS %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figwork	= 1;		% working image
figdiff	= 0;		% raw pixel difference
figcln 	= 0;		% cleaned image
figtop	= 0;		% largest blob
figregions = 0;		% table for detections in each region
figpresence = 0;	% table to track entering/exiting the office
figconnect = 0; 	% connected centers of mass

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Im = imread('../DATA/inspacecam163_2016_02_19_12_51_01.jpg','jpg');
Imback = double(Im);
[MR,MC,Dim] = size(Imback);
decision = zeros(MR,MC); % output array for decision

% RGA parameters
if RGA > 0
  mean = Imback;
  var = repmat(10,[MR MC Dim]);
end

% ARM parameter
if ARM > 0
  Imback_ARM = Imback;
end

% Median/NPP parameters
buf = 10; % size of history buffer
history = zeros(MR,MC,Dim,buf);
if AMF > 0
  for i = 1 : buf
    history(:,:,:,i) = Imback;
  end
elseif (NPP > 0 | CMF > 0)
  Imchr = chromaticity(Imback);
  for i = 1 : buf
    history(:,:,:,i) = Imchr(:,:,:);
  end
end

% Table for number of detections in each region
if (figregions > 0 | figpresence > 0) % figpresence uses td.Data
  fd = figure(figregions);
  set(fd, 'Position', [0 0 450 70]);
  td = uitable(fd,'Data',[0 0 0 0 0],'Position',[0 0 450 70]);
  td.ColumnName = {'Empty','In Office','Desk','Cabinet','Door'};
end

% Parameters for table to track entering/exiting office
if figpresence > 0
  oldcounter = 0; % tracking based on counter differences
  f = figure(figpresence);
  set(f, 'Position', [0 0 200 300]);
  t = uitable(f,'Data',[0 0],'Position',[0 0 200 300]);
  t.ColumnName = {'Entered','Exited'};
  tsize = size(t.Data,1);
end

% Connected red line figure parameters
ccold = -1;
crold = -1;
connects = Im; % output array
if figconnect > 0
  figure(figconnect)
  hold on
  imshow(connects)
end

% Threshold adjustment parameters for when the room is empty
thresh = 80; % lowest threshold for largest blob size
counter = 0; % track consecutive frames without movement
count_thresh = 5; % if there is no movement for this many frames, assume room empty

% loop over all images
for hours = 12 : 16
  for minutes = 0 : 59
    for seconds = 0 : 59
  
      file_name = make_file_name(hours, minutes, seconds);
      % do not load missing name images
      if exist(file_name, 'file') == 0
        continue
      end

      % load image
      Im = (imread(file_name,'jpg')); 
      if figwork > 0
        figure(figwork)
        imshow(Im)
      end
      Imwork = double(Im);

      % inter-frame difference
      if IFD > 0
        decision = ifd(Imwork, Imback, figdiff);
      end

      % approximate median analysis
      if ARM > 0
        [decision, Imback_ARM] = arm(Imwork, Imback_ARM, figdiff);
      end

      % median analysis
      if AMF > 0
        [decision, history] = amf(Imwork, history, figdiff);
      end

      % chromaticity-based median analysis
      if CMF > 0
        [decision, history] = amf_chr(Imwork, history, figdiff);
      end

      % RGA analysis
      if RGA > 0
        [decision, mean, var] = rga(Imwork, mean, var, figdiff);
      end

      % non-parametric
      if NPP > 0
        [decision, history] = npp(Imwork, history, figdiff);
      end

      [cc, cr, thresh, counter, region] = postprocess(decision, thresh, figcln, figtop, figwork, counter);

      % update number of detections in each region
      if figregions > 0
        td.Data(1,region+1) = td.Data(1,region+1) + 1; % update frame count for each region
      end

      % check if the room was exited/entered
      if figpresence > 0
        time = hours*10000 + minutes*100 + seconds;

        if (counter == count_thresh & oldcounter ~= count_thresh & any(td.Data(2:5))) % just exited office, provided any in room detections are present
          t.Data(1,2) = t.Data(1,2) + 1; % update exiting count
          if t.Data(tsize,2) == -1 % check if there is still space in the table
            for row = 1 : tsize
              if t.Data(row,2) == -1;
                t.Data(row,2) = time;
                break;
              end
            end
          else % no space left in table, create new row
	    t.Data = vertcat(t.Data, [-1 time]);
            tsize = size(t.Data,1);
          end

        elseif (oldcounter == count_thresh & counter == 0) % just entered office
          t.Data(1,1) = t.Data(1,1) + 1; % update entering count
          if t.Data(tsize,1) == -1 % check if there is still space in the table
            for row = 1 : tsize
              if t.Data(row,1) == -1;
                t.Data(row,1) = time;
                break;
              end
            end
          else % no space left in table, create new row
	    t.Data = vertcat(t.Data, [time -1]);
            tsize = size(t.Data,1);
          end

        end
        oldcounter = counter;
      end

      % connect centers of mass
      if figconnect > 0
        if (ccold ~= -1 & cc ~= -1) % both centroids present, can draw point and line
          figure(figconnect)
          line([ccold,cc,],[crold,cr],'Color','r');
          viscircles([cc, cr], 1);
        elseif cc ~= -1 % only old centroid is -1, only draw point
          viscircles([cc, cr], 1);
        end
        ccold = cc;
        crold = cr;
      end

      Imback = Imwork;
      pause(0.01)
    end
  end
end 
