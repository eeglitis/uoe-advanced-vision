% post-process: clean up, find biggest blob,
% obtain centroid, estimate location
function [cc, cr, thresh, counter, region] = postprocess(decision, thresh, figcln, figtop, figwork, counter)

cc = -1; % default value for centroid column
cr = -1; % default value for centroid row
region = 0; % set default region as not in office
count_thresh = 5; % if there is no movement for this many frames, assume room empty

% clean up
decision = bwmorph(decision,'majority',5);
decision = bwmorph(decision,'bridge',1);
decision = bwmorph(decision,'close',2);
if figcln > 0
  figure(figcln)
  imshow(decision)
end

% select largest object
labeled = bwlabel(decision,4);
stats = regionprops(labeled,['basic']);
[N,W] = size(stats);
if N < 1
  if counter == count_thresh % raise threshold if no blobs for X frames
    thresh = 400;
  else
    counter = counter + 1;
  end
  return   
end

% do bubble sort (large to small) on regions in case there are more than 1
id = zeros(N);
for i = 1 : N
  id(i) = i;
end
for i = 1 : N-1
  for j = i+1 : N
    if stats(i).Area < stats(j).Area
      tmp = stats(i);
      stats(i) = stats(j);
      stats(j) = tmp;
      tmp = id(i);
      id(i) = id(j);
      id(j) = tmp;
    end
  end
end

% make sure that there is at least 1 big region
if stats(1).Area < thresh
  if counter == count_thresh % raise threshold if not enough movement for X frames
    thresh = 400;
  else
    counter = counter + 1;
  end
  return
else
  region = 1; % large enough object detected - change default region to in office
end
selected = (labeled==id(1));
if figtop > 0
  figure(figtop)
  imshow(selected)
end

% get center of mass and radius of largest blob
centroid = stats(1).Centroid;
radius = sqrt(stats(1).Area/pi);
cc = centroid(1);
cr = centroid(2);

% check if counter needs to be reset
if counter == count_thresh
  if ~(cc > 910 & cc < 1000 & cr < 380) % reset threshold only if movement not in the window
    counter = 0;
    thresh = 80;
  else
    return
  end
else
  counter = 0;
end

% identify relevant region
% check if at the desk
if (cc>375 & cc<725 & cr>190 & cr<560 & radius<185)
  region = 2;
% check if at the cabinet
elseif (cc>1000 & cc<1280 & cr>150 & cr<720)
  region = 3;
% check if at the door
elseif (cc>0 & cc<355 & cr>190 & cr<720)
  region = 4;
end

% draw according regions
if figwork > 0
  figure(figwork)
  hold on
    viscircles([cc, cr], radius);
    viscircles([cc, cr], 1);
    if region == 2
      rectangle('Position',[375 190 350 370],'EdgeColor','b','LineWidth',3);
    elseif region == 3
      rectangle('Position',[1000 150 320 570],'EdgeColor','b','LineWidth',3);
    elseif region == 4
      rectangle('Position',[0 190 350 530],'EdgeColor','b','LineWidth',3);
    end
  hold off
end

return
