fauto = fopen('../100frames/automatic.txt','r');
fman = fopen('../100frames/manual.txt','r');

A = textscan(fauto,'%d %d');
M = textscan(fman,'%d %d');

fclose(fauto);
fclose(fman);

fres = fopen('../100frames/result.txt','w');

res = zeros(0,100);

for i = 1 : 100
  if (A{1}(i) == -1) % automatic detects nothing
    if (M{1}(i) == -1) % both detect nothing
      fprintf(fres,'-1\n');
    else % manual is real - missed detection
      fprintf(fres,'misdetect\n');
    end
  else % automatic has detection
    if (M{1}(i) == -1) % actually nothing - false detection
      fprintf(fres,'false detect\n');
    else % both real, calculate
      point = [A{1}(i),A{2}(i);M{1}(i),M{2}(i)];
      res(i) = pdist(point,'euclidean');
      fprintf(fres,'%.2f\n',res(i));
    end
  end
end

res(1:19) = [];
fmean = mean(res);
fprintf(fres,'\n%.2f\n',fmean);

fclose(fres);
