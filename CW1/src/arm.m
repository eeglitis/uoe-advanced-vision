% approximate median filtering
function [decision, Imback_ARM] = arm(Imwork, Imback_ARM, figdiff)

[MR,MC,Dim] = size(Imwork);

decision = (abs(Imwork(:,:,1) - Imback_ARM(:,:,1)) > 10) ...
         | (abs(Imwork(:,:,2) - Imback_ARM(:,:,2)) > 10) ...
         | (abs(Imwork(:,:,3) - Imback_ARM(:,:,3)) > 10);

% update approximate median
for i = 1 : MR
  for j = 1 : MC
    for k = 1 : Dim
      if Imwork(i,j,k) > Imback_ARM(i,j,k)
        Imback_ARM(i,j,k) = Imback_ARM(i,j,k) + 1;
      elseif Imwork(i,j,k) < Imback_ARM(i,j,k)
        Imback_ARM(i,j,k) = Imback_ARM(i,j,k) - 1;
      end
    end
  end
end

if figdiff > 0
  figure(figdiff)
  imshow(decision)
end

return
