% use probabilistic method of foreground detection
function [decision, bghistory] = npp(Imwork, bghistory, figdiff)

alpha=0.8;
beta=1.2;
tau=0.05;
sigma=0.05;

[rows,cols,dim,old] = size(bghistory);
Imchr = chromaticity(Imwork);

fgprob=zeros(rows,cols);
bgthresh=zeros(rows,cols);
bghistory = circshift(bghistory,-1,4);

for r = 1 : rows
  for c = 1 : cols

    % get chromaticity per pixel
    red = Imchr(r,c,1);
    green = Imchr(r,c,2);
    sat = Imchr(r,c,3);

    % search history at pixel for samples that satisfy saturation ratio test
    prob = 0;
    count = 0;
    for k = 1 : old
      if bghistory(r,c,dim,k) > 0
        ratio = sat/bghistory(r,c,dim,k);
      else
        ratio = sat/10;
      end
      if alpha < ratio && ratio < beta
        count = count+1;
        prob = prob + kernel(sigma,red-bghistory(r,c,1,k))*kernel(sigma,green-bghistory(r,c,2,k));
      end
    end
    if count > 0
      % at least one instance of bg classification
      p_x_b = (prob/count);
      p_b = 0.99;     % rough estimate of % a pixel is BG. Should compute.
      p_x_f= 0.001;   % rough estimate of prob that this foreground pixel value
                      % is chosen. Assumes all FG values possible uniformly and
                      % there are 1000 FG values. Ought to compute this given
                      % some (r,g) samples
      p_b_x = (p_x_b * p_b) / (p_x_f * (1-p_b) + p_x_b * p_b);

      fgprob(r,c)=1-p_b_x; % prob of foreground
      if fgprob(r,c) > (1-tau)
        % colour different enough that foreground
        bgthresh(r,c)=1;
      else
        % found background pixel, so update background model
        bgthresh(r,c)=0;
        bghistory(r,c,1,old) = red;
        bghistory(r,c,2,old) = green;
        bghistory(r,c,3,old) = sat;
      end
    else
      % saturation different enough that foreground
      fgprob(r,c)=1;
      bgthresh(r,c)=1;
    end
  end
end

decision = bgthresh*100;
if figdiff > 0
  figure(figdiff)
  imshow(decision)
end

return
