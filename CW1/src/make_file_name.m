% creates the wanted file name, depending on the time parameters
function file_name = make_file_name(hours, minutes, seconds)

if seconds < 10
    sec_string = ['_0', int2str(seconds)];
else 
    sec_string = ['_',int2str(seconds)];
end


if minutes < 10
    min_string = ['_0', int2str(minutes)];
else 
    min_string = ['_',int2str(minutes)];
end


if hours < 10
    hour_string = ['_0', int2str(hours)];
else 
    hour_string = ['_',int2str(hours)];
end

file_name = ['../DATA/inspacecam163_2016_02_19', hour_string, min_string, sec_string, '.jpg']; 
return;
