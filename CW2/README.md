# README #

### What is this repository for? ###

Advanced Vision coursework 2

### How do I get set up? ###

Extract the .mat from the downloads in coursework description into 'data/'

$ python main.py

Navigate to a browser type the url
http://localhost:3000/<plotname>

The <plotname> with correspnd to one of the datasets
'plane', 'planex', 'planey', 'planez'

LaTeX link: https://www.overleaf.com/8723862mzvxcjzxfwvx


