from fetch import load_mesh
import cv2
from flask import request, Response, Flask
import numpy as np
#import serial # COMMENT OUT ON DICE
from lib.match import phase1, reproject_image_into_polar, fitplane_m
import matplotlib.pyplot as plt
from matplotlib import colors

plots = {"plane": (-1,11, lambda src: src < 1.0 * np.sum(src) / (src.shape[0] * src.shape[1]), True, 2, np.array([300, 200]),np.array([500, 500])),
         "planex": (-1,11, lambda src: src < 1.5 * np.sum(src) / (src.shape[0] * src.shape[1]), False, 0,np.array([50, 200]),np.array([100, 900])),
         "planey": (-1,11, lambda src: src < 1.5 * np.sum(src) / (src.shape[0] * src.shape[1]), False, 1,np.array([100, 50]),np.array([900, 100])),
         "planez": (-1,11, lambda src: src < 1.5 * np.sum(src) / (src.shape[0] * src.shape[1]), True, 2,np.array([300, 200]),np.array([500, 500]))}

application = Flask(__name__, static_path="/static")

def show(plotname):
    startafter, end, thresh, resize, xaxis, orgs, width = plots[plotname]
    start = True
    s = orgs
    w = width
    results = []
    for i, data in enumerate(load_mesh(plotname)):

        if i > startafter and i < end:
            # grey scale image anyway so just call first indices
            frame = data[:, :, 0] * 255.0

            if start:
                last = frame

            # Translation
            dp, conv1 = phase1(last, frame, thresh)
            s = s + dp

            conv2 = np.copy(conv1)
            if resize:
                framep, r_i, theta_i = reproject_image_into_polar(data[:, :, 0:3] * 255.0, (s + w / 2.0).astype("int"))
                framep = framep[:, :, 0]

                if start:
                    lastp = framep

                # Rescale size
                dp, conv2 = phase1(lastp, framep, thresh)
                if dp[1] < 0:
                    w[0]+=int(dp[1] / np.sqrt(1+w[1]/w[0]))
                    w[1]+=int(dp[1] / np.sqrt(1+w[0]/w[1]))
                    s[0]-=int(dp[1] / 2*np.sqrt(1+w[1]/w[0]))
                    s[1]-=int(dp[1] / 2*np.sqrt(1+w[1]/w[0]))

                lastp = framep

            start = False

            # Check if still visable
            if np.all([s + w < np.asarray(frame.shape), 0 < s]):
		plt.close("all")

                e = s+w
                patch = data[s[1]:e[1], s[0]:e[0], 3:]
                # Save stats
                C, rms, residuals = fitplane_m(patch)

		# Create residual array
		heatmat = np.copy(residuals)
		cmap = colors.ListedColormap(['red','yellow','green','blue'])
		bounds=[np.min(residuals),-rms,0,rms,np.max(residuals)]
		norm = colors.BoundaryNorm(bounds, cmap.N)

		# plot residual color map
		error = plt.figure(num="Errors")
		img = plt.imshow(residuals, interpolation='nearest', origin='upper', cmap=cmap, norm=norm)
		plt.colorbar(img, cmap=cmap, norm=norm, boundaries=bounds, ticks=[np.min(residuals),-rms,0,rms,np.max(residuals)])
		error.savefig("demo_results/res_"+plotname+str(i)+".png")

		# Save datapoint
                c = s+(w/2.0)
                datapoint = np.array([c[0], c[1], np.sqrt((w[0]/2.0)**2+(w[1]/2.0)**2), rms])
                results.append(datapoint)
		print "{}_{} / Size {} / RMS {} / {} outliers".format(plotname, i, width, rms, np.sum(residuals[:]>5*rms))
                # Draw box
                cv2.rectangle(frame, tuple(s), tuple(s + w), (255,0,0), 2)
		cv2.circle(frame, tuple(s+(w/2)), 3, (255,0,0), -1)

            last = frame

            img = np.c_[frame,conv1,conv2]
            ret, jpeg = cv2.imencode('.jpg', frame)

            #frame = serial.to_bytes(jpeg) # COMMENT OUT ON DICE
            frame = jpeg.tobytes() # COMMENT OUT ON LOCAL MACHINE
            yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
            cv2.imwrite("demo_results/"+plotname+str(i)+".jpg",last)

    data = np.stack(results)
    np.save("results/results_"+plotname+".npy",data)

@application.route('/<plotname>')
def run(plotname):
    return Response(show(plotname), mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == "__main__":
    application.run(debug=True, host='0.0.0.0', port=3000)
