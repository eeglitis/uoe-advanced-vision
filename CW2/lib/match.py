import cv2
import numpy as np
from scipy.ndimage import map_coordinates

def phase1(src1,src2, thresh):
    cv2.imwrite("results/1.jpg",src1)
    cv2.imwrite("results/2.jpg",src2)
    src1 = np.abs(cv2.imread("results/1.jpg", 0)-255)
    src2 = np.abs(cv2.imread("results/2.jpg", 0)-255)

    src1[thresh(src1)] = 0
    src2[thresh(src2)] = 0

    src1 = np.float32(src1)
    src2 = np.float32(src2)

    #p = cv2.phaseCorrelate(src1,src2) # COMMENT OUT ON DICE
    p, error = cv2.phaseCorrelate(src1,src2) # COMMENT OUT ON LOCAL MACHINE
    r = np.array([p[0],p[1]]).astype("int")
    return r, src2

def fitplane_m(patch):
    surface = np.copy(patch)
    X_p = surface[:, :, 0]
    Y_p = surface[:, :, 1]
    Z_p = surface[:, :, 2]

    patch = patch.reshape(-1,3)
    size = patch.shape[0]

    A = np.c_[patch[:,0], patch[:,1], np.ones(size)]

    C, r, rank, s = np.linalg.lstsq(A,patch[:,2])

    Z = C[0] * X_p + C[1] * Y_p + C[2]
    Xr = X_p - X_p
    Yr = Y_p - Y_p
    Zr = Z_p - Z
    residuals = (-C[0] / C[2]) * Xr + (-C[1] / C[2]) * Yr + (1 / C[2]) * Zr
    rms = np.sqrt(np.average(np.square(residuals)))
    return C, rms, residuals

def index_coords(data, origin=None):
    """Creates x & y coords for the indicies in a numpy array "data".
    "origin" defaults to the center of the image. Specify origin=(0,0)
    to set the origin to the lower left corner of the image."""
    ny, nx = data.shape[:2]
    if origin is None:
        origin_x, origin_y = nx // 2, ny // 2
    else:
        origin_x, origin_y = origin
    x, y = np.meshgrid(np.arange(nx), np.arange(ny))
    x -= origin_x
    y -= origin_y
    return x, y

def cart2polar(x, y):
    r = np.sqrt(x**2 + y**2)
    theta = np.arctan2(y, x)
    return r, theta

def polar2cart(r, theta):
    x = r * np.cos(theta)
    y = r * np.sin(theta)
    return x, y

def reproject_image_into_polar(data, origin):
    ny, nx = data.shape[:2]
    if origin is None:
        origin = (nx//2, ny//2)

    # Determine that the min and max r and theta coords
    x, y = index_coords(data, origin=origin)
    r, theta = cart2polar(x, y)

    # Make a regular (in polar space) grid based on the min and max r & theta
    r_i = np.linspace(r.min(), r.max(), nx)
    theta_i = np.linspace(theta.min(), theta.max(), ny)
    theta_grid, r_grid = np.meshgrid(theta_i, r_i)

    # Project the r and theta grid back into pixel coordinates
    xi, yi = polar2cart(r_grid, theta_grid)
    xi += origin[0] # We need to shift the origin back to
    yi += origin[1] # back to the lower-left corner...
    xi, yi = xi.flatten(), yi.flatten()
    coords = np.vstack((xi, yi)) # (map_coordinates requires a 2xn array)

    # Reproject each band individually and the restack
    bands = []
    for band in data.T:
        zi = map_coordinates(band, coords, order=1)
        bands.append(zi.reshape((nx, ny)))
    output = np.dstack(bands)
    return output, r_i, theta_i


