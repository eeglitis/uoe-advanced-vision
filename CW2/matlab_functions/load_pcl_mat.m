function RGBXYZ = load_pcl_mat(name, num)
	names = sprintf('%s_%02d.mat', name, num);
	load(names)
	[m,n,d] = size(XYZ);
	RGBXYZ = zeros(m,n,6);
	cmap = hsv(256);
	%figure; imshow(Img);
	X = XYZ(:,:,1);
	Y = XYZ(:,:,2);
	Z = XYZ(:,:,3);
	K = isnan(X) | isinf(X) | isnan(Y) | isinf(Y) | isnan(Z) | isinf(Z);
	X(K) = 0;
	Y(K) = 0;
	Z(K) = 0;
	Img = 255*cat(3,Img,Img,Img);
	RGBXYZ(:,:,1) = Img(:,:,1);
	RGBXYZ(:,:,2) = Img(:,:,2);
	RGBXYZ(:,:,3) = Img(:,:,3);
	RGBXYZ(:,:,4) = X;
	RGBXYZ(:,:,5) = Y;
	RGBXYZ(:,:,6) = Z;
end
